.. _readme:

Introduction
============

Minty template project for Python (micro)services.

Getting started
---------------

n/a

More documentation
------------------

Please see the generated documentation via CI for more information about this
module and how to contribute in our online documentation. Open index.html
when you get there:
`<https://gitlab.com/minty-python/minty-infra-sqlalchemy/-/jobs/artifacts/master/browse/tmp/docs?job=qa>`_


Contributing
------------

Please read `CONTRIBUTING.md <https://gitlab.com/minty-python/minty-infra-sqlalchemy/blob/master/CONTRIBUTING.md>`_
for details on our code of conduct, and the process for submitting pull requests to us.

Versioning
----------

We use `SemVer <https://semver.org/>`_ for versioning. For the versions
available, see the
`tags on this repository <https://gitlab.com/minty-python/minty-infra-sqlalchemy/tags/>`_

License
-------
i
Copyright (c) Minty Team and all persons listed in the file `CONTRIBUTORS`

This project is licensed under the EUPL, v1.2. See the `EUPL-1.2.txt` in the
`LICENSES` directory for details.

.. SPDX-FileCopyrightText: 2020 Mintlab B.V.
..
.. SPDX-License-Identifier: EUPL-1.2
